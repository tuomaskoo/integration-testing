/**
 * Padding output to match 2 characters always.
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters
 */
const pad = (hex) => {
    return hex.length === 1 ? "0" + hex : hex;
  };
  
  module.exports = {
    /**
     * Converts RGB values to a hex string
     * @param {number} red 0-255
     * @param {number} green 0-255
     * @param {number} blue 0-255
     * @returns {string} hex value
     */
    rgbToHex: (red, green, blue) => {
      const redHex = pad(red.toString(16));
      const greenHex = pad(green.toString(16));
      const blueHex = pad(blue.toString(16));
      const hex = "#" + redHex + greenHex + blueHex;
      return hex;
    },
  
    /**
     * Converts a hex string to RGB values
     * @param {string} hex hex value
     * @returns {Array<number>} array of RGB values
     */
    hexToRgb: (hex) => {
      hex = hex.replace("#", "");
      const red = parseInt(hex.substring(0, 2), 16);
      const green = parseInt(hex.substring(2, 4), 16);
      const blue = parseInt(hex.substring(4, 6), 16);
      return [red, green, blue];
    }
  };
  