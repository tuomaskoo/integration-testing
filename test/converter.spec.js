// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("RGB to hex conversions", () => {
        it("converts basic colors", () => {
            const redHex = converter.rgbToHex(255, 0, 0); // #ff0000
            expect(redHex).to.equal("#ff0000"); // red hex value

            const blueHex = converter.rgbToHex(0, 0, 255); // #0000ff
            expect(blueHex).to.equal("#0000ff"); // blue hex value
        });
    });

    describe("Hex to RGB conversions", () => {
        it("converts basic colors", () => {
            const redRgb = converter.hexToRgb("#ff0000"); // 255, 0, 0
            expect(redRgb).to.deep.equal([255, 0, 0]); // red RGB value

            const blueRgb = converter.hexToRgb("#0000ff"); // 0, 0, 255
            expect(blueRgb).to.deep.equal([0, 0, 255]); // blue RGB value
        });
    });
});
